import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { appReducers } from './app.store';
import { CoreModule } from './core/core.module';
import { EmployeeModule } from './feature/employee/employee.module';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        StoreModule.forRoot(appReducers),
        EffectsModule.forRoot([]),
        CoreModule, EmployeeModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
