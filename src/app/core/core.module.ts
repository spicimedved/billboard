import { NgModule } from '@angular/core';
import { IBillboardClient } from './http/ibillboard-client';

@NgModule({
    providers: [
        IBillboardClient,
    ],
})
export class CoreModule {
}
