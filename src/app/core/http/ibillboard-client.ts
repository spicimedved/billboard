import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

@Injectable()
export class IBillboardClient {

    private url: string;

    constructor(private http: HttpClient) {
        this.url = environment.billboardApiUrl;
    }

    getPositions() {
        return this.http.get(this.url + '/positions');
    }
}
