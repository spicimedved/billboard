export interface IEmployee {
    uuid: string;
    name: string;
    surname: string;
    position?: string;
    birthday: Date;
    contractStart?: Date;
    contractEnd?: Date;
    contractIndefinite: boolean;
}