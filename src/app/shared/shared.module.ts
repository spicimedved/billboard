import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataGridFilterComponent } from './data-grid/data-grid-filter.component';
import { DataGridRowEditableComponent } from './data-grid/data-grid-row-editable.component';
import { DataGridComponent } from './data-grid/data-grid.component';
import { BoolToTextPipe } from './data-grid/pipe/bool-to-text.pipe';

@NgModule({
    declarations: [
        DataGridComponent,
        DataGridRowEditableComponent,
        DataGridFilterComponent,
        BoolToTextPipe,
    ],
    imports: [
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
    ],
    exports: [
        FormsModule,
        CommonModule,
        DataGridComponent,
    ],
    providers: [
    ],
})
export class SharedModule {
}
