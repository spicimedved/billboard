import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'boolToText'
})
export class BoolToTextPipe implements PipeTransform {

    transform(value: boolean, args?: any): any {
        return value === true ? 'Yes' : value === false ? 'No' : '[Not set]';
    }

}
