import { TestBed, inject } from '@angular/core/testing';

import { ErrorMessageResolver } from './error-message-resolver.service';

describe('ErrorMessageResolver', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ErrorMessageResolver]
    });
  });

  it('should be created', inject([ErrorMessageResolver], (service: ErrorMessageResolver) => {
    expect(service).toBeTruthy();
  }));
});
