import { Injectable } from '@angular/core';

@Injectable()
export class ErrorMessageResolver {

    private map: Map<string, string>;

    constructor() {
        this.map = new Map([
            ['required', 'Value is required!']
        ]);
    }

    public getMessage(error: string, defaultMessage = '') {
        if (this.map.has(error)) {
            return this.map.get(error);
        }
        return defaultMessage || 'Value is invalid!';
    }
}
