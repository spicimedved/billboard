import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn } from '@angular/forms';
import 'rxjs/add/operator/takeWhile';
import { Observable } from 'rxjs/Observable';
import { IDataGridColumn, IDataGridItem, NEW_ITEM_INDEX_KEY } from './data-grid.component';
import { DataTransformerMap } from './data-transformer/data-transformer-map';
import { DataTransformerMapFactory } from './data-transformer/data-transformer-map-factory';
import { ErrorMessageResolver } from './error-message-resolver/error-message-resolver.service';

@Component({
    selector: '[app-data-grid-row-editable]',
    templateUrl: './data-grid-row-editable.component.html',
    styleUrls: ['./data-grid-row-editable.component.scss'],
    providers: [
        ErrorMessageResolver
    ]
})
export class DataGridRowEditableComponent implements OnInit, OnDestroy {
    @Input() item: IDataGridItem = {};
    @Input() columns: IDataGridColumn[] = [];
    @Input() validators: ValidatorFn[] = [];

    @Output() save: EventEmitter<IDataGridItem> = new EventEmitter();
    @Output() cancel: EventEmitter<IDataGridItem> = new EventEmitter();
    @Output() delete: EventEmitter<IDataGridItem> = new EventEmitter();

    public form: FormGroup;
    public errors: {[key: string]: string[]};
    public choices: {[key: string]: string[]};
    public inEditMode = false;

    private alive = true;
    private saveClicked = false;
    private transformerMap: DataTransformerMap;

    constructor(
        private errorMessageResolver: ErrorMessageResolver,
        private transformerMapFactory: DataTransformerMapFactory
    ) {
        this.errors = {};
        this.choices = {};
        this.transformerMap = transformerMapFactory.getInstance();
    }

    ngOnInit() {
        this.saveClicked = false;
        this.resolveColumnOptions();
        this.toggleEditMode(this.item.hasOwnProperty(NEW_ITEM_INDEX_KEY));
    }

    ngOnDestroy() {
        this.alive = false;
    }

    onEdit() {
        this.saveClicked = false;
        this.toggleEditMode(true);
    }

    onCancel() {
        this.toggleEditMode(false);
        this.cancel.emit(this.item);
    }

    onSave() {
        this.saveClicked = true;
        if (this.form.invalid) {
            this.updateFormErrors();
            return;
        }
        this.toggleEditMode(false);
        this.loadFormValues();
        this.save.emit(this.item);
    }

    onDelete() {
        this.toggleEditMode(false);
        this.delete.emit(this.item);
    }

    private createForm() {
        const form = new FormGroup({}, this.validators || []);

        this.columns.forEach((column: IDataGridColumn) => {
            form.addControl(column.field, new FormControl(
                this.transformValue(this.item[column.field], column.type) || '',
                column.validators || [],
            ));
        });

        return form;
    }

    private loadFormValues() {
        const controls = this.form.controls;
        return Object.keys(controls).forEach((key) => {
            this.item[key] = this.reverseTransformValue(
                controls[key].value, this.getColumn(key).type
            );
        });
    }

    private resetFormValues() {
        const controls = this.form.controls;
        Object.keys(controls).forEach((key) => {
            controls[key].setValue(this.transformValue(
                this.item[key], this.getColumn(key).type
            ));
        });
        this.errors = {};
    }

    private transformValue(value, type) {
        if (this.transformerMap.has(type)) {
            const transformer = this.transformerMap.get(type);
            return transformer.transform(value);
        }
        return value;
    }

    private reverseTransformValue(value, type) {
        if (this.transformerMap.has(type)) {
            const transformer = this.transformerMap.get(type);
            return transformer.reverseTransform(value);
        }
        if (type === 'boolean') {
            return !!value;
        }
        return value;
    }

    private subscribeOnFormStatusChanges() {
        this.form.statusChanges
            .takeWhile(() => this.alive)
            .subscribe(() => {
                this.updateFormErrors();
            });
    }

    private updateFormErrors() {
        Object.keys(this.form.controls).forEach((key) => {
            const errors = this.errors[key] = [];
            const control = this.form.controls[key];

            if (control.valid) {
                return;
            }

            if ((this.saveClicked || control.dirty) && control.invalid) {
                errors.push(Object.keys(control.errors).map((validatorKey) => {
                    return this.getValidatorMessage(control.errors, validatorKey);
                }));
            }
        });

        if (this.form.invalid && this.form.errors) {
            Object.keys(this.form.errors).forEach((validatorKey) => {
                this.errors[validatorKey].push(
                    this.getValidatorMessage(this.form.errors, validatorKey)
                );
            });
        }
    }

    private resolveColumnOptions() {
        this.columns.forEach((column) => {
            if (column.type === 'choice' && column.choices instanceof Observable) {
                column.choices
                    .takeWhile(() => this.alive)
                    .subscribe((values) => {
                        this.choices[column.field] = values;
                    });
            }
        });
    }

    private toggleEditMode(inEditMode: boolean) {
        if (inEditMode) {
            if (!(this.form instanceof FormGroup)) {
                this.form = this.createForm();
                this.subscribeOnFormStatusChanges();
            } else {
                this.resetFormValues();
            }
        }
        this.inEditMode = inEditMode;
    }

    private getValidatorMessage(errors, validatorKey: string) {
        const message = (errors && errors[validatorKey]) ? errors[validatorKey].message : null;

        if (!message) {
            return this.errorMessageResolver.getMessage(validatorKey);
        }
        return message;
    }

    private getColumn(key) {
        return this.columns.find((column) => column.field === key);
    }
}
