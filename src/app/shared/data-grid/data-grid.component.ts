import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { ValidatorFn } from '@angular/forms/src/directives/validators';
import { Observable } from 'rxjs/Observable';
import { DataComparatorMap } from './data-comparator/data-comparator-map';
import { DataComparatorMapFactory } from './data-comparator/data-comparator-map-factory';
import { IDataGridFilter } from './data-grid-filter.component';
import { DataTransformerMapFactory } from './data-transformer/data-transformer-map-factory';

export const NEW_ITEM_INDEX_KEY = '__index__';

export interface IDataGridItem {
    [key: string]: any;
}

export interface IDataGridColumn {
    type: string;
    field: string;
    title: string;
    sortable?: boolean;
    choices?: any[]|Observable<any>;
    filterable?: boolean;
    validators?: ValidatorFn[];
    validatorMessages?: {[key: string]: string};
}

@Component({
    selector: 'app-data-grid',
    templateUrl: './data-grid.component.html',
    styleUrls: ['./data-grid.component.scss'],
    providers: [
        DataTransformerMapFactory, DataComparatorMapFactory
    ],
})
export class DataGridComponent implements OnInit, OnChanges {

    @Input() items: IDataGridItem[] = [];
    @Input() filter: IDataGridFilter = {};
    @Input() columns: IDataGridColumn[] = [];
    @Input() validators: ValidatorFn[] = [];
    @Input() prototypeItem: IDataGridItem = {};

    @Output() add: EventEmitter<IDataGridItem> = new EventEmitter();
    @Output() save: EventEmitter<IDataGridItem> = new EventEmitter();
    @Output() delete: EventEmitter<IDataGridItem> = new EventEmitter();
    @Output() filtered: EventEmitter<IDataGridItem> = new EventEmitter();

    public newItems: IDataGridItem[] = [];
    public listedItems: IDataGridItem[] = [];
    public filterVisible = false;

    private comparatorMap: DataComparatorMap;
    private sortAscendant: { [key: string]: boolean };
    private appliedFilter: IDataGridFilter;
    private newItemIndex: number;

    constructor(private comparatorMapFactory: DataComparatorMapFactory) {
        this.sortAscendant = {};
        this.appliedFilter = null;
        this.comparatorMap = comparatorMapFactory.getInstance();
        this.newItemIndex = 0;
    }

    ngOnChanges(changes: SimpleChanges) {
        this.filterItems();
    }

    ngOnInit() {
        this.filterVisible = this.columns.some((column) => {
            return column.filterable;
        });
        this.filterItems(this.filter);
    }

    sort(column: IDataGridColumn) {
        const type = column.type !== 'choice' ? column.type : 'string';

        if (!this.comparatorMap.has(type)) {
            return;
        }

        const sign = this.sortingSign(column.field);
        const comparator = this.comparatorMap.get(type);

        this.listedItems.sort((a, b) => {
            return sign * comparator.compare(a[column.field], b[column.field]);
        });
    }

    onFilterChange(filter: IDataGridFilter) {
        this.filterItems(filter);
    }

    addItem() {
        this.newItems.push(this.createNewItem());
    }

    onSave(item: IDataGridItem) {
        if (item.hasOwnProperty(NEW_ITEM_INDEX_KEY)) {
            this.removeFromNewItems(item);
            delete item[NEW_ITEM_INDEX_KEY];
        }
        this.save.emit(item);
    }

    onCancel(item: IDataGridItem) {
        this.removeFromNewItems(item);
    }

    onDelete(item: IDataGridItem) {
        this.delete.emit(item);
    }

    private filterItems(filter?: IDataGridFilter) {
        if (filter) {
            this.appliedFilter = filter;
        }
        if (!this.appliedFilter) {
            return this.listedItems = this.items;
        }
        this.listedItems = this.items.filter(this.applyFilterRules.bind(this));
        this.filtered.emit(this.listedItems);

        return this.listedItems;
    }

    private applyFilterRules(item) {
        for (const [field, value] of Object.entries(this.appliedFilter)) {
            const column = this.columns.find(c => c.field === field);

            if (!this.compareValues(item[field], value, column.type)) {
                return false;
            }
        }
        return true;
    }

    private compareValues(itemValue, filterValue, type): boolean {
        if (filterValue === null || filterValue === '' || filterValue === undefined) {
            return true;
        }

        if (itemValue === null || itemValue === '' || itemValue === undefined) {
            return false;
        }

        switch (type) {
            case 'date':
                return new Date(filterValue).getTime() === itemValue.getTime();
            case 'boolean':
                return !!+filterValue === itemValue;
            default:
                return RegExp(filterValue, 'i').test(itemValue);
        }
    }

    private createNewItem() {
        const item = {};
        item[NEW_ITEM_INDEX_KEY] = ++this.newItemIndex;

        if (this.prototypeItem) {
            return Object.assign(item, this.prototypeItem);
        }

        this.columns.forEach((column) => {
            item[column.field] = '';
        });
        return item;
    }

    private sortingSign(field: string): number {
        if (!this.sortAscendant.hasOwnProperty(field)) {
            this.sortAscendant = {};
            this.sortAscendant[field] = true;
        } else {
            this.sortAscendant[field] = !this.sortAscendant[field];
        }

        return this.sortAscendant[field] ? 1 : -1;
    }

    private removeFromNewItems(canceledItem: IDataGridItem) {
        this.newItems = this.newItems.filter((item: IDataGridItem) => {
            return item[NEW_ITEM_INDEX_KEY] !== canceledItem[NEW_ITEM_INDEX_KEY];
        });
    }
}
