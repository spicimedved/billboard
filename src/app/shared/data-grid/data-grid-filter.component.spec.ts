import { CommonModule } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DataGridFilterComponent } from './data-grid-filter.component';
import { DataGridRowEditableComponent } from './data-grid-row-editable.component';
import { DataGridComponent } from './data-grid.component';
import { BoolToTextPipe } from './pipe/bool-to-text.pipe';

describe('DataGridFilterComponent', () => {
    let component: DataGridFilterComponent;
    let fixture: ComponentFixture<DataGridFilterComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                DataGridComponent,
                DataGridRowEditableComponent,
                DataGridFilterComponent,
                BoolToTextPipe,
            ],
            imports: [
                FormsModule,
                CommonModule,
                ReactiveFormsModule,
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DataGridFilterComponent);
        component = fixture.componentInstance;
        component.filter = [];
        component.columns = [];
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
