import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IDataGridColumn } from './data-grid.component';

export interface IDataGridFilter {
    [key: string]: any;
}

@Component({
    selector: '[app-data-grid-filter]',
    templateUrl: './data-grid-filter.component.html',
    styleUrls: ['./data-grid-filter.component.scss'],
})
export class DataGridFilterComponent implements OnInit {

    @Input() columns: IDataGridColumn[] = [];
    @Input() filter: IDataGridFilter = {};

    @Output() filterChange: EventEmitter<IDataGridFilter> = new EventEmitter();

    public currentFilter: IDataGridFilter = {};

    ngOnInit() {
        this.initGridFilter();
    }

    onChange() {
        this.filterChange.emit(this.currentFilter);
    }

    clearFilter() {
        this.clearGridFilter();
        this.filterChange.emit(this.currentFilter);
    }

    private initGridFilter() {
        this.columns.forEach((column: IDataGridColumn) => {
            if (column.filterable) {
                this.currentFilter[column.field] = (this.filter && this.filter[column.field]) || null;
            }
        });
    }

    private clearGridFilter() {
        for (const [field, value] of Object.entries(this.currentFilter)) {
            this.currentFilter[field] = null;
        }
    }
}
