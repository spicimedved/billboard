import { CommonModule } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataGridFilterComponent } from './data-grid-filter.component';

import { DataGridRowEditableComponent } from './data-grid-row-editable.component';
import { DataGridComponent } from './data-grid.component';
import { DataTransformerMapFactory } from './data-transformer/data-transformer-map-factory';
import { ErrorMessageResolver } from './error-message-resolver/error-message-resolver.service';
import { BoolToTextPipe } from './pipe/bool-to-text.pipe';

describe('DataGridRowEditableComponent', () => {
    let component: DataGridRowEditableComponent;
    let fixture: ComponentFixture<DataGridRowEditableComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                DataGridComponent,
                DataGridRowEditableComponent,
                DataGridFilterComponent,
                BoolToTextPipe,
            ],
            imports: [
                FormsModule,
                CommonModule,
                ReactiveFormsModule,
            ],
            providers: [
                ErrorMessageResolver,
                DataTransformerMapFactory,
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DataGridRowEditableComponent);
        component = fixture.componentInstance;
        component.columns = [];
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
