import { CommonModule } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { CoreModule } from '../../core/core.module';
import { DataComparatorMapFactory } from './data-comparator/data-comparator-map-factory';
import { DataGridFilterComponent } from './data-grid-filter.component';
import { DataGridRowEditableComponent } from './data-grid-row-editable.component';

import { DataGridComponent } from './data-grid.component';
import { BoolToTextPipe } from './pipe/bool-to-text.pipe';

describe('DataGridComponent', () => {
    let component: DataGridComponent;
    let fixture: ComponentFixture<DataGridComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                DataGridComponent,
                DataGridRowEditableComponent,
                DataGridFilterComponent,
                BoolToTextPipe,
            ],
            imports: [
                CoreModule,
                FormsModule,
                CommonModule,
                ReactiveFormsModule,
            ],
            providers: [
                DataComparatorMapFactory
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DataGridComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should render table with employees', () => {
        component.columns = [
            {
                type: 'string',
                field: 'name',
                title: 'Name',
                sortable: true,
                filterable: true,
                validators: [Validators.required],
            },
            {
                type: 'string',
                field: 'surname',
                title: 'Surname',
                sortable: true,
                filterable: true,
                validators: [Validators.required],
            },
        ];

        component.items = [
            {
                uuid: '1234',
                name: 'Petr',
                surname: 'Jaša',
                position: 'full-stack developer',
                birthday: new Date('1983-05-15'),
                contractIndefinite: true,
            },
        ];

        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('table tr').textContent).toContain('Petr');
    });
});
