
export interface IDataComparator<T> {

    compare(first: T, second: T): number;

    dataType(): string;
}
