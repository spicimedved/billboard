import { IDataComparator } from './data-comparator';

export class StringComparator implements IDataComparator<string> {

    compare(first: string, second: string): number {
        return first.localeCompare(second);
    }

    dataType(): string {
        return 'string';
    }
}
