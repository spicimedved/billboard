import { IDataComparator } from './data-comparator';

export class BooleanComparator implements IDataComparator<boolean> {

    compare(first: boolean, second: boolean): number {

        if (first === true && second === false) {
            return 1;
        }

        if (first === false && second === true) {
            return -1;
        }

        return 0;
    }

    dataType(): string {
        return 'boolean';
    }
}
