import { Injectable } from '@angular/core';
import { BooleanComparator } from './boolean-comparator';
import { DataComparatorMap } from './data-comparator-map';
import { DateComparator } from './date-comparator';
import { StringComparator } from './string-comparator';


@Injectable()
export class DataComparatorMapFactory {

    private comparatorMap: DataComparatorMap;

    getInstance() {
        if (!this.comparatorMap) {
            this.comparatorMap = new DataComparatorMap();

            const booleanComparator = new BooleanComparator();
            this.comparatorMap.add(booleanComparator.dataType(), booleanComparator);

            const stringComparator = new StringComparator();
            this.comparatorMap.add(stringComparator.dataType(), stringComparator);

            const dateComparator =  new DateComparator();
            this.comparatorMap.add(dateComparator.dataType(), dateComparator);
        }
        return this.comparatorMap;
    }
}
