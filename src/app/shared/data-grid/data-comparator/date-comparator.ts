import { IDataComparator } from './data-comparator';

export class DateComparator implements IDataComparator<Date> {

    compare(first: Date, second: Date): number {
        if (!(first instanceof Date)) {
            return -1;
        }

        if (!(second instanceof Date)) {
            return 1;
        }

        if (first.getTime() > second.getTime()) {
            return 1;
        }

        if (first.getTime() < second.getTime()) {
            return -1;
        }

        return 0;
    }

    dataType(): string {
        return 'date';
    }
}
