import { IDataComparator } from './data-comparator';

export class DataComparatorMap implements IterableIterator<IDataComparator<any>> {

    private sorters: {
        [key: string]: IDataComparator<any>
    };

    constructor() {
        this.sorters = {};
    }

    add(key: string, transformer: IDataComparator<any>) {
        this.sorters[key] = transformer;
    }

    get(key: string): IDataComparator<any> {
        return this.sorters[key];
    }

    has(key: string): boolean {
        return this.sorters.hasOwnProperty(key);
    }

    remove(key: string) {
        delete this.sorters[key];
    }

    next(value?: any): IteratorResult<IDataComparator<any>> {
        throw new Error('Method not implemented.');
    }

    [Symbol.iterator](): IterableIterator<IDataComparator<any>> {
        return this;
    }
}
