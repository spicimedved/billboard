import { IDataTransformer } from './data-transformer';

export class DataTransformerMap implements IterableIterator<IDataTransformer<any, any>> {

    private transformers: {
        [key: string]: IDataTransformer<any, any>
    };

    constructor() {
        this.transformers = {};
    }

    add(key: string, transformer: IDataTransformer<any, any>) {
        this.transformers[key] = transformer;
    }

    get(key: string): IDataTransformer<any, any> {
        return this.transformers[key];
    }

    has(key: string): boolean {
        return this.transformers.hasOwnProperty(key);
    }

    remove(key: string) {
        delete this.transformers[key];
    }

    next(value?: any): IteratorResult<IDataTransformer<any, any>> {
        throw new Error('Method not implemented.');
    }

    [Symbol.iterator](): IterableIterator<IDataTransformer<any, any>> {
        return this;
    }
}
