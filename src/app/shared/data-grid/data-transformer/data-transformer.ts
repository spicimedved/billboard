
export interface IDataTransformer<Input, Output> {

    transform(data: Input): Output;

    reverseTransform(data: Output): Input;

    dataType(): string;
}
