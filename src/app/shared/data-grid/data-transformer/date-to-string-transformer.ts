import { IDataTransformer } from './data-transformer';

export class DateToStringTransformer implements IDataTransformer<Date, string> {

    transform(date: Date): string {
        if (!(date instanceof Date)) {
            return '';
        }
        const iso = date.toISOString();
        return iso.substring(0, iso.indexOf('T'));
    }

    reverseTransform(value: string): Date {
        const regex = /\d{4}-\d{2}-\d{2}/;
        if (!regex.test(value)) {
            return null;
        }
        return new Date(value);
    }

    dataType() {
        return 'date';
    }
}
