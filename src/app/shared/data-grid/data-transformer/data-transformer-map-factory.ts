import { Injectable } from '@angular/core';
import { DataTransformerMap } from './data-transformer-map';
import { DateToStringTransformer } from './date-to-string-transformer';

@Injectable()
export class DataTransformerMapFactory {

    private transformerMap: DataTransformerMap;

    getInstance() {
        if (!this.transformerMap) {
            this.transformerMap = new DataTransformerMap();
            this.transformerMap.add('date', new DateToStringTransformer());
        }
        return this.transformerMap;
    }
}
