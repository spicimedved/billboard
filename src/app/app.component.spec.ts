import { TestBed, async } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { AppComponent } from './app.component';
import { appReducers } from './app.store';
import { CoreModule } from './core/core.module';
import { EmployeeModule } from './feature/employee/employee.module';
import { StoreModule, Store, combineReducers, ReducerManager } from '@ngrx/store';
import { employeeReducer } from './feature/employee/employee.store';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent
            ],
            imports: [
                BrowserModule,
                StoreModule.forRoot({
                    ...appReducers,
                    // 'employee': combineReducers(employeeReducer)
                }),
                EffectsModule.forRoot([]),
                CoreModule, EmployeeModule
            ]
        }).compileComponents();
    }));
    it('should create the app', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));
    it('should render title in a h1 tag', async(() => {
        const fixture = TestBed.createComponent(AppComponent);
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('Employees');
    }));
});
