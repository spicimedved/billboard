import { Component, OnDestroy, OnInit } from '@angular/core';
import { ValidatorFn, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs/Subscription';
import { IAppState, StoreAction } from '../../../app.store';
import { IEmployee } from '../../../model/employee';
import { IDataGridFilter } from '../../../shared/data-grid/data-grid-filter.component';
import { IDataGridColumn, IDataGridItem } from '../../../shared/data-grid/data-grid.component';
import {
    CREATE_EMPLOYEE,
    DELETE_EMPLOYEE,
    getEmployeesSelector,
    getPositionsSelector,
    LOAD_POSITIONS,
    UPDATE_EMPLOYEE
} from '../employee.store';
import { birthdayValidator, contractDateValidator, contractEndValidator, contractStartValidator } from '../employee.validators';

@Component({
    selector: 'app-employee-list',
    templateUrl: './employee-list.component.html',
    styleUrls: ['./employee-list.component.scss'],
})
export class EmployeeListComponent implements OnInit, OnDestroy {

    public columns: IDataGridColumn[] = [];
    public employees: IEmployee[] = [];
    public validators: ValidatorFn[] = [];
    public listedItems: IDataGridItem[] = [];
    public initialFilter: IDataGridFilter = {};

    private subscriptions: Subscription[];

    constructor(private store: Store<IAppState>) {
        this.employees = [];
        this.listedItems = [];
        this.subscriptions = [];
        this.validators = [contractDateValidator];
    }

    ngOnInit() {
        this.columns = this.getColumnConfiguration();
        this.initialFilter = {};
        this.store.dispatch(new StoreAction(LOAD_POSITIONS));

        this.subscriptions.push(
            this.store.select(getEmployeesSelector).subscribe((employees) => {
                this.employees = employees || [];
            })
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach((subscription) => subscription.unsubscribe());
    }

    onSave(employee: IEmployee) {
        const actionType = employee.uuid ? UPDATE_EMPLOYEE : CREATE_EMPLOYEE;
        this.store.dispatch(new StoreAction(actionType, employee));
    }

    onDelete(employee: IEmployee) {
        this.store.dispatch(new StoreAction(DELETE_EMPLOYEE, employee));
    }

    onFilter(items: IDataGridItem[]) {
        this.listedItems = items;
    }

    private getColumnConfiguration(): IDataGridColumn[] {
        return [
            {
                type: 'string',
                field: 'name',
                title: 'Name',
                sortable: true,
                filterable: true,
                validators: [Validators.required],
            },
            {
                type: 'string',
                field: 'surname',
                title: 'Surname',
                sortable: true,
                filterable: true,
                validators: [Validators.required],
            },
            {
                type: 'choice',
                field: 'position',
                title: 'Job title',
                choices: this.store.select(getPositionsSelector),
                sortable: true,
                filterable: true,
            },
            {
                type: 'date',
                field: 'birthday',
                title: 'Day of birth',
                sortable: true,
                filterable: true,
                validators: [Validators.required, birthdayValidator],
            },
            {
                type: 'date',
                field: 'contractStart',
                title: 'Start',
                sortable: true,
                filterable: true,
                validators: [contractStartValidator],
            },
            {
                type: 'date',
                field: 'contractEnd',
                title: 'End',
                sortable: true,
                filterable: true,
                validators: [contractEndValidator],
            },
            {
                type: 'boolean',
                field: 'contractIndefinite',
                title: 'Indefinite period',
                sortable: true,
                filterable: true,
            },
        ];
    }
}
