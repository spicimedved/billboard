import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EffectsModule } from '@ngrx/effects';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import { appReducers, StoreAction } from '../../../app.store';
import { IBillboardClient } from '../../../core/http/ibillboard-client';
import { SharedModule } from '../../../shared/shared.module';
import { EmployeeContractAlertComponent } from '../employee-contract-alert/employee-contract-alert.component';
import { EmployeeEffects, employeeReducer, EMPLOYEES_LOADED, IEmployeeState } from '../employee.store';

import { EmployeeListComponent } from './employee-list.component';

describe('EmployeeListComponent', () => {
    let component: EmployeeListComponent;
    let fixture: ComponentFixture<EmployeeListComponent>;
    let store: Store<IEmployeeState>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                EmployeeListComponent,
                EmployeeContractAlertComponent,
            ],
            imports: [
                HttpClientModule,
                StoreModule.forRoot({
                    ...appReducers,
                    'employee': combineReducers(employeeReducer)
                }),
                EffectsModule.forRoot([EmployeeEffects]),
                SharedModule,
            ],
            providers: [
                IBillboardClient
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        store = TestBed.get(Store);
        spyOn(store, 'dispatch').and.callThrough();

        fixture = TestBed.createComponent(EmployeeListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should load employees', () => {
        expect(component.employees.length).toBe(0);
    });
});
