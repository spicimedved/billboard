import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/mergeMap';
import { Observable } from 'rxjs/Observable';
import { IAppState, StoreAction } from '../../app.store';
import { IBillboardClient } from '../../core/http/ibillboard-client';

const InitEmployeeState = {
    positions: [],
    employees: [
        {
            uuid: '1234',
            name: 'Petr',
            surname: 'Jaša',
            position: 'full-stack developer',
            birthday: new Date('1983-05-15'),
            contractIndefinite: true,
        },
        {
            uuid: '4321',
            name: 'Karel',
            surname: 'Novák',
            position: 'front-end developer',
            birthday: new Date('1989-04-20'),
            contractIndefinite: true,
        },
        {
            uuid: '8888',
            name: 'Jan',
            surname: 'Kobliha',
            position: 'help desk',
            birthday: new Date('1992-09-17'),
            contractEnd: new Date('2018-11-30'),
            contractIndefinite: false,
        }
    ],
};

export interface IEmployeeState {
    positions: string[];
    employees: any[];
}

export const getPositionsSelector = (state: IAppState) => state.employee.positions;
export const getEmployeesSelector = (state: IAppState) => state.employee.employees;

export const LOAD_POSITIONS = 'LOAD_POSITIONS';
export const POSITIONS_LOADED = 'POSITIONS_LOADED';
export const POSITIONS_LOADING_FAILED = 'POSITIONS_LOADING_FAILED';

export const CREATE_EMPLOYEE = 'CREATE_EMPLOYEE';
export const UPDATE_EMPLOYEE = 'UPDATE_EMPLOYEE';
export const DELETE_EMPLOYEE = 'DELETE_EMPLOYEE';
export const LOAD_EMPLOYEES = 'LOAD_EMPLOYEES';
export const EMPLOYEES_LOADED = 'EMPLOYEES_LOADED';

export function employeeReducer(state: IEmployeeState = InitEmployeeState, action: StoreAction) {
    switch (action.type) {
        case POSITIONS_LOADED:
            return {
                ...state,
                positions: action.payload.positions,
            };

        case EMPLOYEES_LOADED:
            return {
                ...state,
                employees: action.payload,
            };

        case CREATE_EMPLOYEE:
            action.payload.uuid = Math.random();
            return {
                ...state,
                employees: [...state.employees, action.payload],
            };

        case UPDATE_EMPLOYEE:
            return {
                ...state,
                employees: state.employees.map((employee) => {
                    if (employee.uuid === action.payload.uuid) {
                        employee = Object.assign({}, action.payload);
                    }
                    return employee;
                }),
            };

        case DELETE_EMPLOYEE:
            return {
                ...state,
                employees: state.employees.filter(employee => employee.uuid !== action.payload.uuid),
            };

        default:
            return state;
    }
}

@Injectable()
export class EmployeeEffects {

    constructor(private api: IBillboardClient,
                private actions$: Actions) {
    }

    @Effect()
    employees$: Observable<Action> = this.actions$.ofType(LOAD_EMPLOYEES)
        .merge(() => ({type: EMPLOYEES_LOADED, payload: []}));


    @Effect()
    positions$: Observable<Action> = this.actions$.ofType(LOAD_POSITIONS)
        .mergeMap(() =>
            this.api.getPositions()
                .map(data => ({type: POSITIONS_LOADED, payload: data}))
                .catch(() => Observable.of({type: POSITIONS_LOADING_FAILED}))
        );

}
