import { AbstractControl, FormGroup } from '@angular/forms';

export function birthdayValidator(control: AbstractControl) {
    const value = parseDate(control.value);
    const invalid = {birthday: {message: 'Employee is too young. Should be at least 15 years old.'}};

    const limit = new Date();
    limit.setFullYear(limit.getFullYear() - 15);
    if (value.getTime() >= limit.getTime()) {
        return invalid;
    }

    return null;
}

export function contractStartValidator(control: AbstractControl) {
    return validateDateBiggerThenToday(control.value, {
        contractStart: {message: 'Date has to be bigger then today\'s date'}
    });
}

export function contractEndValidator(control: AbstractControl) {
    return validateDateBiggerThenToday(control.value, {
        contractEnd: {message: 'Date has to be bigger then today\'s date'}
    });
}

export function contractDateValidator(group: FormGroup) {
    const endDate = parseDate(group.value.contractEnd);
    const startDate = parseDate(group.value.contractStart);
    const contractIndefinite = group.value.contractIndefinite;

    if (contractIndefinite) {
        if (!(endDate instanceof Date) || !(startDate instanceof Date)) {
            return null;
        }

        if (isNaN(endDate.getTime()) || isNaN(endDate.getTime())) {
            return null;
        }
    } else {
        if (!(endDate instanceof Date) || isNaN(endDate.getTime())) {
            return {contractEnd: {message: 'End date has to be set for contract with definite period!'}};
        }
        if (!(startDate instanceof Date) || isNaN(startDate.getTime())) {
            return null;
        }
    }

    if (!isDateBigger(endDate, startDate)) {
        return {contractEnd: {message: 'End date has to be bigger than start date!'}};
    }

    return null;

}

function parseDate(value) {
    if (!(value instanceof Date)) {
        value = new Date(value);
    }
    return value;
}

function isDateBigger(first: Date, second: Date) {
    if (!(first instanceof Date)) {
        throw Error('');
    }
    return first.getTime() > second.getTime();
}

function validateDateBiggerThenToday(value, invalid) {
    value = parseDate(value);

    if (!(value instanceof Date)) {
        return null;
    }

    if (isNaN(value.getTime())) {
        return null;
    }

    if (!isDateBigger(value, new Date())) {
        return invalid;
    }

    return null;
}
