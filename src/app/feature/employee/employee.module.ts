import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '../../shared/shared.module';
import { EmployeeContractAlertComponent } from './employee-contract-alert/employee-contract-alert.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeEffects, employeeReducer } from './employee.store';

@NgModule({
    declarations: [
        EmployeeListComponent,
        EmployeeContractAlertComponent,
    ],
    imports: [
        HttpClientModule,
        StoreModule.forFeature('employee', employeeReducer),
        EffectsModule.forFeature([EmployeeEffects]),
        SharedModule,
    ],
    exports: [
        EmployeeListComponent,
    ],
    providers: [
    ],
})
export class EmployeeModule {
}
