import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeContractAlertComponent } from './employee-contract-alert.component';

describe('EmployeeContractAlertComponent', () => {
    let component: EmployeeContractAlertComponent;
    let fixture: ComponentFixture<EmployeeContractAlertComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [EmployeeContractAlertComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(EmployeeContractAlertComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
