import { Component, Input, OnChanges, OnInit, ViewEncapsulation } from '@angular/core';
import { IEmployee } from '../../../model/employee';

@Component({
    selector: 'app-employee-contract-alert',
    templateUrl: './employee-contract-alert.component.html',
    styleUrls: ['./employee-contract-alert.component.scss'],
})
export class EmployeeContractAlertComponent implements OnChanges {

    @Input() employees: IEmployee[] = [];

    public endingEmployees: IEmployee[] = [];

    ngOnChanges() {
        this.endingEmployees = this.filterEmployeesWithEndingContract();
    }

    private filterEmployeesWithEndingContract(): IEmployee[] {
        if (!this.employees) {
            return [];
        }

        return this.employees.filter((employee: IEmployee) => {
            if (employee.contractIndefinite) {
                return false;
            }

            if (!(employee.contractEnd instanceof Date)) {
                return false;
            }

            return this.compareByYearMonth(this.createNextMonthDate(), employee.contractEnd) >= 0;
        });
    }

    private createNextMonthDate() {
        const nextMonth = new Date();
        nextMonth.setMonth(nextMonth.getMonth() + 1);
        return nextMonth;
    }

    private compareByYearMonth(a: Date, b: Date) {
        const aString = this.convertToYearMonthString(a);
        const bString = this.convertToYearMonthString(b);
        return aString.localeCompare(bString);
    }

    private convertToYearMonthString(date: Date) {
        const month = date.getMonth();
        return date.getFullYear() + '-' + (month < 10 ? '0' : '') + month;
    }

}
