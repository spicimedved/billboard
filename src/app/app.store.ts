import { Action, ActionReducerMap } from '@ngrx/store';
import { employeeReducer, IEmployeeState } from './feature/employee/employee.store';

export interface IAppState {
    employee: IEmployeeState;
}

export class StoreAction implements Action {
    constructor(public readonly type, public readonly payload: any = {}) {
    }
}

export const appReducers: ActionReducerMap<IAppState> = {
    employee: employeeReducer,
};
